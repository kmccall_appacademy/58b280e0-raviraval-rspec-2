def measure(run_times = 1)
  time_sum = 0
  run_times.times do
    first_time = Time.now
    yield
    change_time = Time.now - first_time
    time_sum += change_time
  end
  time_sum.fdiv(run_times)
end
