def reverser
  res = []
  yield.split(" ").each do |word|
    res << word.reverse
  end
  res.join(" ")
end

def adder(add_amount = 1)
  start_number = yield
  start_number + add_amount
end

def repeater(repeat_times = 1, &block)
  repeat_times.times &block
end
